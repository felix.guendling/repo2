#include "repo2/repo2.h"

#include "repo3/repo3.h"

namespace repo2 {

int repo2() { return 2 + repo3::repo3(); }

}  // namespace repo2